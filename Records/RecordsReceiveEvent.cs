﻿using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	[System.Serializable]
	public class RecordsReceiveEvent : UnityEvent<string, int, string> {
	}
}

