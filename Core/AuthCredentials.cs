﻿using System;
using UnityEngine;

namespace UniDs {
	public class AuthCredentials {
		public string username;
		public string password;

		public AuthCredentials() { }
		public AuthCredentials(string name, string pass) {
			username = name;
			password = pass;
		}

		public string ToJson() {
			return JsonUtility.ToJson (this);
		}
	}
}
