﻿using System;
using System.Runtime.Serialization;

namespace UniDs {
	[Serializable]
	public class DeepStreamException : Exception {
		public DeepStreamException() {

		}

		public DeepStreamException(string message)
			: base(message) {
		}

		public DeepStreamException(string message, Exception innerException)
			: base(message, innerException) {

		}

		public DeepStreamException(string error, string message)
			: base(error+" - "+message) {
		}

		protected DeepStreamException(SerializationInfo info, StreamingContext context)
			: base(info, context) {

		}
	}
}