﻿using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	[System.Serializable]
	public class RecordsPatchEvent : UnityEvent<string, int, string, object> {
	}
}

