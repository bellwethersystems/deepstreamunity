﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Globalization;

namespace UniDs {
	public class MessageFormatter {

		public static Message[] DeserializeMessages(string text) {
			string[] groups = text.Split (Constants.GroupSeperator);
			List<Message> messages = new List<Message> ();

			for (int i = 0; i < groups.Length - 1; i++) {
				if (!string.IsNullOrEmpty (groups [i])) {
					messages.Add (new Message (groups [i]));
				}
			}
			
			return messages.ToArray ();
		}

		public static string SerializeData<T>(T data) {
			if (data is string) {
				return Constants.Types.STRING + data.ToString();
			} else if (data is bool) {
				if (bool.Parse(data.ToString()))
					return Constants.Types.TRUE.ToString();
				else
					return Constants.Types.FALSE.ToString();
			} else if (data is int || data is double || data is float || data is decimal) {
				return Constants.Types.NUMBER + data.ToString().Replace(',', '.');
			} else if ((data as object) == null) {
				return Constants.Types.NULL.ToString();
			} else {
				try {
					return null;
//					return Constants.Types.OBJECT + TODO: serialize data as json (data)
				} catch {
					return Constants.Types.UNDEFINED.ToString();
				}
			}
		}

		public static KeyValuePair<Type, object> DeserializeData(string data) {
			var payload = data.Substring(1);

			switch (data[0]) {
			case Constants.Types.STRING:
				return new KeyValuePair<Type, object>(typeof(string), payload);

			case Constants.Types.NUMBER:
				return new KeyValuePair<Type, object>(typeof(double), double.Parse(payload, CultureInfo.InvariantCulture));

			case Constants.Types.TRUE:
				return new KeyValuePair<Type, object>(typeof(bool), true);

			case Constants.Types.FALSE:
				return new KeyValuePair<Type, object>(typeof(bool), false);

			case Constants.Types.NULL:
				return new KeyValuePair<Type, object>(typeof(object), null);

			case Constants.Types.OBJECT:
				return new KeyValuePair<Type, object>(typeof(string), payload);
//				return new KeyValuePair<Type, object>(typeof(object), JsonConvert.DeserializeObject(evtData));

			default:
				return new KeyValuePair<Type, object>(typeof(string), payload);
			}
		}

		public static T FromJson<T>(string json) {
			// Remove deepstream object dtype prefix (if present)
			string objectJson = json.StartsWith (Constants.Types.OBJECT.ToString()) ? json.Substring (1) : json;

			return UnityEngine.JsonUtility.FromJson<T> (objectJson);
		}

		public static T[] FromJsonList<T>(string json) {
			// Remove deepstream object dtype prefix (if present)
			string objectJson = json.StartsWith (Constants.Types.OBJECT.ToString()) ? json.Substring (1) : json;

			string obj = JsonArrayEnvelope (objectJson);
			JsonListWrapper<T> wrapper = UnityEngine.JsonUtility.FromJson<JsonListWrapper<T>> (obj);
			return wrapper.Items;
		}

		public static string JsonArrayEnvelope(string json) {
			return string.Format("{{ \"Items\": {0}}}", json);
		}

		public static string ToJson(object obj) {
			return UnityEngine.JsonUtility.ToJson (obj);
		}

		public static string ToJsonList<T>(T[] array) {
			JsonListWrapper<T> wrapper = new JsonListWrapper<T>();
			wrapper.Items = array;
			return UnityEngine.JsonUtility.ToJson (wrapper);
		}

		[System.Serializable]
		private class JsonListWrapper<T> {
			public T[] Items;
		}
	}
}