﻿using System;

namespace UniDs {
	public abstract class Service : IDisposable {

		private Connection _connection;
		public Connection Connection { 
			get { return _connection; }
		}

		protected Service(Connection connection) {
			_connection = connection;
		}

		protected void ThrowIfArgumentMissing(string arg) {
			if (string.IsNullOrEmpty(arg))
				throw new ArgumentNullException(arg);
		}

		protected void ThrowIfConnectionNotOpened() {
			if (Connection.State != ConnectionState.OPEN)
				throw new DeepStreamException("Login required first");
		}

		public virtual void OnMessage(Message message) {
		}

		public virtual void Dispose() {
			Dispose (true);
			GC.SuppressFinalize (this);
		}

		protected virtual void Dispose(bool disposing) {
		}
	}
}
