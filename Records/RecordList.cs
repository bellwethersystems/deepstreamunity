﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UniDs {
	public class RecordList {

		public string ListName { get; private set; }
		public Record Record { get; private set; }
		private List<Record> Records;

		public UnityEvent OnReady;
		public UnityEvent OnChanged;
		public UnityEvent OnDestroyed;

		public RecordList(string listName, Record record) {
			ListName = listName;
			Records = new List<Record> ();
			Record = record;

			OnReady = new UnityEvent ();
			OnChanged = new UnityEvent ();
			OnDestroyed = new UnityEvent ();
		}

		public Record[] GetEntries() {
			return Records.ToArray ();
		}

		public bool Contains(string name) {
			foreach (Record record in Records) {
				if (record.RecordName == name)
					return true;
			}
			return false;
		}

		public void ListReadyHandler() {
			string[] names = MessageFormatter.FromJsonList<string> (Record.Data);
			UnityEngine.Debug.Log (string.Format ("RecordList.ListReadyHandler : {0}", names.Length));

			Records.Clear ();
			foreach (string name in names) {
				Record record = new Record (name);
				Records.Add (record);
			}
		}

		public void ListChangedHandler() {
			UnityEngine.Debug.Log ("RecordList.ListChangedHandler");
		}

		public void Discard() {
			OnDestroyed.Invoke ();
		}
	}
}
