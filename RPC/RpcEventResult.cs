﻿using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	[System.Serializable]
	public class RpcEventResult {
		public string MessageId;
		public string RpcName;
		public string Payload;

		public RpcEventResult() {
			MessageId = string.Empty;
			RpcName = string.Empty;
			Payload = string.Empty;
		}

		public RpcEventResult(string id, string name) {
			MessageId = id;
			RpcName = name;
			Payload = string.Empty;
		}

		public RpcEventResult(string id, string name, string message) {
			MessageId = id;
			RpcName = name;
			Payload = message;
		}

		public override string ToString() {
			return string.Format ("{0} ({1}): {2}", RpcName, MessageId, Payload);
		}
	}
}