﻿namespace UniDs {
	static class Constants {
		internal const char RecordSeperator = (char)31;
		internal const char GroupSeperator = (char)30;
		internal const string NewlineString = "\r\n";

		internal static class Errors {
			public const string CONNECTION_ERROR = "CONNECTION_ERROR";
			public const string INVALID_AUTH_DATA = "INVALID_AUTH_DATA";
			public const string TOO_MANY_AUTH_ATTEMPTS = "TOO_MANY_AUTH_ATTEMPTS";
			public const string MESSAGE_PARSE_ERROR = "MESSAGE_PARSE_ERROR";
			public const string UNSOLICITED_MESSAGE = "UNSOLICITED_MESSAGE";
			public const string NOT_SUBSCRIBED = "NOT_SUBSCRIBED";
			public const string NOT_LISTENING = "NOT_LISTENING";
			public const string NO_RPC_PROVIDER = "NO_RPC_PROVIDER";
			public const string VERSION_EXISTS = "VERSION_EXISTS";
			public const string RECORD_NOT_FOUND = "RECORD_NOT_FOUND";
		}

		internal static class Types {
			public const char STRING = 'S';
			public const char OBJECT = 'O';
			public const char NUMBER = 'N';
			public const char NULL = 'L';
			public const char TRUE = 'T';
			public const char FALSE = 'F';
			public const char UNDEFINED = 'U';
		}
	}
}