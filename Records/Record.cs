﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	public class Record {
		public string RecordName;
		public string Path;
		public string Data;
		public int RecordVersion;

		public bool IsReady { get; internal set; }
		public bool IsDestroyed { get; internal set; }

		public UnityEvent OnReady;
		public UnityEvent OnChanged;
		public UnityEvent OnDestroyed;
		public UnityEvent OnError;

		public Record(string name) {
			RecordName = name;
			RecordVersion = 0;
			IsReady = false;
			IsDestroyed = false;

			OnReady = new UnityEvent ();
			OnChanged = new UnityEvent ();
			OnDestroyed = new UnityEvent ();
			OnError = new UnityEvent ();
		}

		public Record(string name, string path, int version) {
			Path = path;
			RecordVersion = version;
			IsReady = false;
			IsDestroyed = false;
		}

		public T FromJson<T>() {
			if (string.IsNullOrEmpty (Data)) {
				return (T)Activator.CreateInstance (typeof(T));
			} else {
				return MessageFormatter.FromJson<T> (Data);
			}
		}

		public void ReceiveEventHandler(string name, int version, string data) {
			UnityEngine.Debug.Log ("Record.ReceiveEventHandler (" + name + " v"+version.ToString()+")  ->  " + data);
			bool becameReady = !IsReady;
			IsReady = true;

			RecordVersion = Math.Max (version, 1);
			Data = data;
			if (becameReady) {
				OnReady.Invoke ();
			} else {
				OnChanged.Invoke ();
			}
		}

		public void PatchEventHandler(string name, int version, string path, object value) {
			UnityEngine.Debug.Log ("Record.PatchEventHandler");
			RecordVersion = version;

			OnChanged.Invoke ();
		}

		public void DeleteEventHandler(string name) {
			IsDestroyed = true;
			OnDestroyed.Invoke ();
		}

		public void ErrorEventHandler(string name, string message, string version) {
			UnityEngine.Debug.Log (string.Format ("Record.ErrorEventHandler: {0}, {1}, {2}", name, message, version));

			if (message == Constants.Errors.VERSION_EXISTS) {
				int iVersion = RecordVersion;
				if (int.TryParse (version, out iVersion)) {
					RecordVersion = iVersion;
				}
			}
		}
	}
}
