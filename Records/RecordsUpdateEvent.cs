﻿using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	[System.Serializable]
	public class RecordsUpdateEvent : UnityEvent<string, int, object> {
	}
}

