﻿using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	[System.Serializable]
	public class EventsAcknowledgeEvent : UnityEvent<string, string> {
	}
}