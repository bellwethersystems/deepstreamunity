﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UniDs {
	public class RecordsService : Service {
		
		private Dictionary<string, RecordsErrorEvent> OnRecordsError;
		private Dictionary<string, RecordsDeleteEvent> OnRecordsDelete;
		private Dictionary<string, RecordsReceiveEvent> OnRecordsReceive;
		private Dictionary<string, RecordsPatchEvent> OnRecordsPatch;
		private Dictionary<string, RecordsAcknowledgeEvent> OnRecordsAcknowledge;

		public RecordsService(Connection connection) 
			: base(connection) {
			OnRecordsError = new Dictionary<string, RecordsErrorEvent> ();
			OnRecordsDelete = new Dictionary<string, RecordsDeleteEvent> ();
			OnRecordsReceive = new Dictionary<string, RecordsReceiveEvent> ();
			OnRecordsPatch = new Dictionary<string, RecordsPatchEvent> ();
			OnRecordsAcknowledge = new Dictionary<string, RecordsAcknowledgeEvent> ();
		}

		public override void OnMessage(Message message) {
			if (message.Action == Action.ACK) {
				if (message.AcknowledgedAction == Action.DELETE) {
				} else if (message.AcknowledgedAction == Action.CREATEORREAD) {
				} else if (message.AcknowledgedAction == Action.LISTEN) {
				} else if (message.AcknowledgedAction == Action.UNLISTEN) {
				}

				foreach (KeyValuePair<string, RecordsAcknowledgeEvent> kv in OnRecordsAcknowledge) {
					if (message.Name.StartsWith (kv.Key)) {
						kv.Value.Invoke ();
					}
				}
				
			} else if (message.Action == Action.READ) {
				foreach (KeyValuePair<string, RecordsReceiveEvent> kv in OnRecordsReceive) {
					if (message.Name.StartsWith (kv.Key)) {
						string json = message.Payload [0] as string;
						int version = VersionStringToInt (message.Uid);
						kv.Value.Invoke (message.Name, version, json);
					}
				}

			} else if (message.Action == Action.UPDATE) {
				foreach (KeyValuePair<string, RecordsReceiveEvent> kv in OnRecordsReceive) {
					if (message.Name.StartsWith (kv.Key)) {
						string json = message.Payload [0] as string;
						int version = VersionStringToInt (message.Uid);
						kv.Value.Invoke (message.Name, version, json);
					}
				}

			} else if (message.Action == Action.PATCH) {
				foreach (KeyValuePair<string, RecordsPatchEvent> kv in OnRecordsPatch) {
					if (message.Name.StartsWith (kv.Key)) {
						string path = message.Payload [0] as string;
						if (path.Contains (".")) {
							string err = string.Format ("Nested Record patch path ({0}) not implemented", path);
							throw new NotImplementedException (err);
						}

						object value = message.Payload [1];
						int version = VersionStringToInt (message.Uid);
						kv.Value.Invoke (message.Name, version, path, value);
					}
				}

			} else if (message.Action == Action.DELETE) {
				foreach (KeyValuePair<string, RecordsDeleteEvent> kv in OnRecordsDelete) {
					if (message.Name == kv.Key) {
						kv.Value.Invoke (message.Name);
						kv.Value.RemoveAllListeners ();
					} else if (message.Name.StartsWith (kv.Key)) {
						kv.Value.Invoke (message.Name);
					}
				}

				if (OnRecordsDelete.ContainsKey (message.Name))
					OnRecordsDelete.Remove (message.Name);

			} else if (message.Action == Action.ERROR) {
				UnityEngine.Debug.Log ("RecordService ERROR: " + message.ToString () + " ... v" + message.Uid);
				if (OnRecordsError.ContainsKey (message.Name)) {
					string error = message.Payload [2] as string;
					OnRecordsError [message.Name].Invoke (message.Name, error, message.Uid);
				}
			}
		}

		public Record GetRecord(string name) {
			ThrowIfArgumentMissing (name);

			Message message = new Message (Topic.RECORD, Action.CREATEORREAD);
			message.Payload = new object[1]{ name };
			Connection.Write (message);

			Record record = new Record(name);
			AddRecordCallbacks (record);

			return record;
		}

		public void UpdateRecord(string name, int version, string data) {
			ThrowIfArgumentMissing (name);
			ThrowIfArgumentMissing (data);

			Message message = new Message (Topic.RECORD, Action.UPDATE);
			message.Payload = new object[3]{ name, version, data };
			Connection.Write (message);
		}

		public void PatchRecord(string name, int version, string path, object value) {
			ThrowIfArgumentMissing (name);
			ThrowIfArgumentMissing (path);

			Message message = new Message (Topic.RECORD, Action.PATCH);
			message.Payload = new object[4]{ name, version, path, value };
			Connection.Write (message);
		}

		public void DeleteRecord(string name) {
			ThrowIfArgumentMissing (name);

			Message message = new Message (Topic.RECORD, Action.DELETE);
			message.Payload = new object[1]{ name };
			Connection.Write (message);
		}

		public RecordList GetList(string name) {
			ThrowIfArgumentMissing (name);

			Record record = GetRecord (name);
			RecordList list = new RecordList (name, record);
			record.OnReady.AddListener (list.ListReadyHandler);
			record.OnChanged.AddListener (list.ListChangedHandler);
			return list;
		}

		public void GetEntries(RecordList list) {
			if (!list.Record.IsReady)
				return;
			
			string[] names = list.Record.FromJson<string[]> ();
		}

		public void Discard(Record record) {
			record.DeleteEventHandler (record.RecordName);
			Unlisten (record.RecordName);
		}

		public void Listen(string pattern) {
			ThrowIfArgumentMissing (pattern);

			Message message = new Message (Topic.RECORD, Action.LISTEN);
			message.Payload = new object[1]{ pattern };
			Connection.Write (message);
		}

		public void Unlisten(string pattern) {
			ThrowIfArgumentMissing (pattern);

			Message message = new Message (Topic.RECORD, Action.UNLISTEN);
			message.Payload = new object[1]{ pattern };
			Connection.Write (message);
		}

		private void AddRecordCallbacks(Record record) {
			if (OnRecordsError.ContainsKey (record.RecordName)) {
				OnRecordsError[record.RecordName].AddListener (record.ErrorEventHandler);
			} else {
				RecordsErrorEvent ev = new RecordsErrorEvent ();
				ev.AddListener (record.ErrorEventHandler);
				OnRecordsError.Add (record.RecordName, ev);
			}

			if (OnRecordsReceive.ContainsKey (record.RecordName)) {
				OnRecordsReceive[record.RecordName].AddListener (record.ReceiveEventHandler);
			} else {
				RecordsReceiveEvent ev = new RecordsReceiveEvent ();
				ev.AddListener (record.ReceiveEventHandler);
				OnRecordsReceive.Add (record.RecordName, ev);
			}

			if (OnRecordsPatch.ContainsKey (record.RecordName)) {
				OnRecordsPatch[record.RecordName].AddListener (record.PatchEventHandler);
			} else {
				RecordsPatchEvent ev = new RecordsPatchEvent ();
				ev.AddListener (record.PatchEventHandler);
				OnRecordsPatch.Add (record.RecordName, ev);
			}

			if (OnRecordsDelete.ContainsKey (record.RecordName)) {
				OnRecordsDelete[record.RecordName].AddListener (record.DeleteEventHandler);
			} else {
				RecordsDeleteEvent ev = new RecordsDeleteEvent ();
				ev.AddListener (record.DeleteEventHandler);
				OnRecordsDelete.Add (record.RecordName, ev);
			}
		}

		private void RemoveRecordCallbacks(Record record) {
			if (OnRecordsError.ContainsKey (record.RecordName))
				OnRecordsError.Remove (record.RecordName);
			
			if (OnRecordsDelete.ContainsKey (record.RecordName))
				OnRecordsDelete.Remove (record.RecordName);

			if (OnRecordsReceive.ContainsKey (record.RecordName))
				OnRecordsReceive.Remove (record.RecordName);
			
			if (OnRecordsPatch.ContainsKey (record.RecordName))
				OnRecordsPatch.Remove (record.RecordName);

			if (OnRecordsAcknowledge.ContainsKey (record.RecordName))
				OnRecordsAcknowledge.Remove (record.RecordName);
		}

		private int VersionStringToInt(string version) {
			int i;
			int.TryParse(version, out i);
			return i;
		}

		private void Dispose(bool disposing) {
			if (disposing) {
				ArrayList errKeys = new ArrayList(OnRecordsError.Keys);
				foreach (string key in errKeys) {
					OnRecordsError [key] = null;
					OnRecordsError.Remove (key);
				}

				ArrayList delKeys = new ArrayList(OnRecordsDelete.Keys);
				foreach (string key in delKeys) {
					OnRecordsDelete [key] = null;
					OnRecordsDelete.Remove (key);
				}

				ArrayList recKeys = new ArrayList(OnRecordsReceive.Keys);
				foreach (string key in recKeys) {
					OnRecordsReceive [key] = null;
					OnRecordsReceive.Remove (key);
				}

				ArrayList patchKeys = new ArrayList(OnRecordsPatch.Keys);
				foreach (string key in patchKeys) {
					OnRecordsPatch [key] = null;
					OnRecordsPatch.Remove (key);
				}

				ArrayList ackKeys = new ArrayList(OnRecordsAcknowledge.Keys);
				foreach (string key in ackKeys) {
					OnRecordsAcknowledge [key] = null;
					OnRecordsAcknowledge.Remove (key);
				}
			}
		}
	}
}