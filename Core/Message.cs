﻿using System;
using System.Collections.Generic;
using System.Text;

// REF: https://deepstream.io/info/protocol/all-messages/

namespace UniDs {
	public class Message {
		public readonly Topic Topic;
		public readonly Action Action;
		public readonly Action AcknowledgedAction;
		public string Uid;
		public string Name;
		public object[] Payload;

		public static string CreateUid() {
			return Guid.NewGuid ().ToString ("N");
		}

		public Message(Topic topic, Action action) {
			Topic = topic;
			Action = action;
			Payload = new object[0];
			Uid = Message.CreateUid ();
		}

		public Message(Topic topic, Action action, object[] payload) {
			Topic = topic;
			Action = action;
			Payload = payload;
			Uid = Message.CreateUid ();
		}

		public Message(string text) {
			string[] parts = text.Split (Constants.RecordSeperator);
			ThrowIfMissingParts (parts, 2);

			int protocolParts = 2;
			Topic = new Topic (parts [0]);
			Action = new Action (parts [1]);
			if (Action == Action.ACK && parts.Length > 2) {
				AcknowledgedAction = new Action (parts [2]);
				protocolParts++;
			}
			Payload = new string[parts.Length - protocolParts];
			Array.Copy(parts, protocolParts, Payload, 0, Payload.Length);

			ProcessMessage ();
		}

		protected void ThrowIfMissingParts(object[] parts, int size) {
			if (parts.Length < size) {
				throw new DeepStreamException (Constants.Errors.MESSAGE_PARSE_ERROR);
			}
		}

		public bool IsError() {
			return Action == Action.ERROR;
		}

		public string ToPackage() {
			return ToPackage (false);
		}

		public string ToPackage(bool serialize) {
			StringBuilder builder = new StringBuilder (1 + 4 + Payload.Length * 2);
			builder.Append (Topic);
			builder.Append (Constants.RecordSeperator);
			builder.Append (Action);
			builder.Append (Constants.RecordSeperator);

			for (int i = 0; i < Payload.Length; i++) {
				if (serialize) {
					string data = MessageFormatter.SerializeData (Payload [i]);
					builder.Append (data);
				} else {
					string str = Payload [i].ToString ();
					builder.Append (str);
				}
				builder.Append (Constants.RecordSeperator);
			}

			builder[builder.Length - 1] = Constants.GroupSeperator;
			return builder.ToString ();
		}

		private void ProcessMessage() {
			string[] parts = Payload as string[];

			if (Topic == Topic.CONNECTION) {
				// do nothing
			} else if (Topic == Topic.AUTH) {
				ProcessAuthMessage (parts);
			} else if (Topic == Topic.EVENT) {
				ProcessEventMessage (parts);
			} else if (Topic == Topic.RECORD) {
				ProcessRecordMessage (parts);
			} else if (Topic == Topic.RPC) {
				ProcessRPCMessage (parts);
			} else {
				UnityEngine.Debug.Log ("ERROR unkown topic " + Topic);
				throw new DeepStreamException (Constants.Errors.MESSAGE_PARSE_ERROR);
			}
		}

		private void ProcessAuthMessage(string[] parts) {
			if (Action == Action.ACK) {
			} else if (Action == Action.ERROR) {
				string error = parts [0];
				if (error == Constants.Errors.INVALID_AUTH_DATA) {
				} else if (error == Constants.Errors.TOO_MANY_AUTH_ATTEMPTS) {
				}
				Name = error;
			}
		}

		private void ProcessEventMessage(string[] parts) {
			if (Action == Action.ACK) {
				ThrowIfMissingParts (parts, 1);
				Name = parts [0];

			} else if (Action == Action.EVENT) {
				ThrowIfMissingParts (parts, 2);

				Name = parts [0];
				string data = parts [2] as string;
				Payload = new object[1]{ data };
			}
		}

		private void ProcessRecordMessage(string[] parts) {
			if (Action == Action.ACK) {

			} else if (Action == Action.READ) {
				ThrowIfMissingParts (parts, 3);

				Name = parts [0];
				Uid = parts [1]; // version number
				string data = parts [2] as string;
				Payload = new object[1]{ data };

			} else if (Action == Action.CREATEORREAD) {
				ThrowIfMissingParts (parts, 1);

				Name = parts [0];

			} else if (Action == Action.UPDATE) {
				ThrowIfMissingParts (parts, 3);

				Name = parts [0];
				Uid = parts [1]; // version number
				string data = parts [2] as string;
				Payload = new object[1]{ data };

			} else if (Action == Action.PATCH) {
				ThrowIfMissingParts (parts, 4);

				Name = parts [0];
				Uid = parts [1]; // version number

				string path = parts [2];
				var objectData = MessageFormatter.DeserializeData (parts [3]);
				Payload = new object[2]{ path, objectData };

			} else if (Action == Action.DELETE) {
				ThrowIfMissingParts (parts, 1);

				Name = parts [0];

			} else if (Action == Action.ERROR) {
				ThrowIfMissingParts (parts, 1);

				string error = parts [0];
				if (error == Constants.Errors.VERSION_EXISTS) {
					Uid = parts [2]; // version number
				} else if (error == Constants.Errors.RECORD_NOT_FOUND) {
				}
				Name = parts [1];
			} else {
			}
		}

		private void ProcessRPCMessage(string[] parts) {
			if (Action == Action.ACK) {
				ThrowIfMissingParts (parts, 2);
				Name = parts [0];
				Uid = parts [1]; // rpc id

			} else if (Action == Action.RESPONSE) {
				ThrowIfMissingParts (parts, 3);
				Name = parts [0];
				Uid = parts [1]; // rpc id
				string data = parts [2];
				Payload = new object[1]{ data };

			} else if (Action == Action.REQUEST) {
				throw new NotImplementedException ("RPC Request not implemented");

			} else if (Action == Action.REJECTION) {
				ThrowIfMissingParts (parts, 2);
				Name = parts [0];
				Uid = parts [1]; // rpc id

				string msg = string.Format ("{0} for {1}", Constants.Errors.NO_RPC_PROVIDER, Name);
				throw new DeepStreamException (msg);
			} else if (Action == Action.ERROR) {
				ThrowIfMissingParts (parts, 3);
				string error = parts [0];
				if (error == Constants.Errors.NO_RPC_PROVIDER) {
				}
				Name = parts [1];
				Uid = parts [2]; // rpc id
			}
		}

		public override string ToString() {
			if (Payload.Length > 0) {
				return ToPackage ().Replace (Constants.RecordSeperator, "|".ToCharArray () [0]);
			} else {
				StringBuilder builder = new StringBuilder (string.Format ("{0}|{1}", Topic, Action));
				if (!string.IsNullOrEmpty(Name)) {
					builder.Append (string.Format ("|{0}", Name));
				}
				return builder.ToString ();
			}
		}
	}
}