﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using WebSocketSharp;
using UnityEngine.Events;

namespace UniDs {
	public class Connection : IDisposable {

		public MessageEvent OnMessage;
		public UnityEvent OnDisconnect;

		private readonly WebSocket Ws;
		public readonly string Host;
		public readonly int Port;
		public readonly string Path;
		public ConnectionState State { get; internal set; }

		public string Url {
			get {
				if (Path == null) {
					return string.Format ("ws://{0}:{1}", Host, Port);
				} else {
					return string.Format ("ws://{0}:{1}/{2}", Host, Port, Path);
				}
			}
		}

		public Connection(string host, int port, string path) {
			State = ConnectionState.NONE;
			Host = host;
			Port = port;
			Path = path;

			OnMessage = new MessageEvent ();
			OnDisconnect = new UnityEvent ();

			if (string.IsNullOrEmpty (host))
				throw new ArgumentNullException ("Missing tcp socket host");

			if (string.IsNullOrEmpty (host))
				throw new ArgumentNullException ("Missing tcp socket port");
			
			try {
				SetState(ConnectionState.AWAITING_CONNECTION);

				UnityEngine.Debug.Log("URL "+Url);
				Ws = new WebSocket (Url);
				Ws.OnClose += OnCloseHandler;
				Ws.OnError += OnErrorHandler;
				Ws.OnMessage += OnMessageHandler;
				Ws.OnOpen += OnOpenHandler;


			} catch (Exception e) {
				UnityEngine.Debug.Log (string.Format ("Connection error: {0}", e.ToString ()));
				State = ConnectionState.ERROR;
				throw new DeepStreamException (Constants.Errors.CONNECTION_ERROR, e);
			}
		}

		public void OnOpenHandler(object sender, EventArgs args) {
			
		}

		public void OnCloseHandler(object sender, WebSocketSharp.CloseEventArgs args) {
			TransitionState (ConnectionState.CLOSED, true);
			OnDisconnect.Invoke ();
		}

		public void OnErrorHandler(object sender, WebSocketSharp.ErrorEventArgs args) {
			UnityEngine.Debug.Log (string.Format ("Websocket error: {0}, {1}", args.ToString (), args.Message));
			UnityEngine.Debug.Log (args.Exception.ToString ());
		}

		public void OnMessageHandler(object sender, WebSocketSharp.MessageEventArgs args) {
			UnityEngine.Debug.Log (string.Format ("Websocket message: {0}", args.Data));
			Message[] messages = MessageFormatter.DeserializeMessages (args.Data);

			foreach (Message message in messages) {
				OnMessage.Invoke (message);
			}
		}

		public bool OnServerCertificateValidationHandler(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
			// TODO: check pinned ssl certificate
			return true;
		}

		public bool Connect() {
			try {
				Ws.Connect ();

				if (Ws.IsConnected) {
					SetState(ConnectionState.CHALLENGING);
				}

			} catch (Exception e) {
				UnityEngine.Debug.Log (string.Format ("Connection.Connect error: {0}", e.ToString ()));
				State = ConnectionState.ERROR;
				throw new DeepStreamException (Constants.Errors.CONNECTION_ERROR, e);
			}

			return IsListening ();
		}

		public bool IsListening() {
			return Ws.IsConnected;
		}

		public void Write(Message message) {
			UnityEngine.Debug.Log ("Writing: " + message.ToString ());

			Ws.SendAsync (message.ToPackage (), (completed) => {
				//
			});
		}

		public void SetState(ConnectionState state) {
			// REF: https://deepstream.io/tutorials/core/writing-a-client/connection-state-diagram.png
			if (state == ConnectionState.AWAITING_CONNECTION) {
				TransitionState (state, State == ConnectionState.NONE || State == ConnectionState.CLOSED);
			} else if (state == ConnectionState.AWAITING_AUTHENTICATION) {
				TransitionState (state, State == ConnectionState.AWAITING_CONNECTION || State == ConnectionState.RECONNECTING);
			} else if (state == ConnectionState.AUTHENTICATING) {
				TransitionState (state, State == ConnectionState.AWAITING_AUTHENTICATION);
			} else if (state == ConnectionState.OPEN) {
				TransitionState (state, State == ConnectionState.AUTHENTICATING);
			} else if (state == ConnectionState.RECONNECTING) {
				TransitionState (state, State == ConnectionState.OPEN);
			} else if (state == ConnectionState.ERROR) {
				TransitionState (state, true);
			}
		}

		private void TransitionState(ConnectionState state, bool condition) {
			if (condition) {
				State = state;
			} else {
				string msg = string.Format ("State transition not allowed: {0} to {1}", State, state);
				throw new DeepStreamException (msg);
			}
		}

		public void Disconnect() {
			Disconnect (true);
		}

		public void Disconnect(bool disconnecting) {
			if (disconnecting) {
				Ws.Close ();
			}

			TransitionState (ConnectionState.CLOSED, true);
		}

		public void Dispose() {
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		private void Dispose(bool disposing) {
			if (disposing) {
				OnMessage.RemoveAllListeners ();
				OnDisconnect.RemoveAllListeners ();
				Ws.Close ();
			}
		}
	}

}
