﻿using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	[System.Serializable]
	public class RecordsErrorEvent : UnityEvent<string, string, string> {
	}
}