# Deepstream Unity

A codebase for working with deepstream.io (https://deepstream.io) servers in Unity. Implementatino based on official deepstream docs and reference to the JS client. The code was lifted directly from another project, and is is meant for implementation reference, not as production code.

The WebSocketSharp (https://github.com/sta/websocket-sharp) library is used for websocket negotiation, etc. The intent is to minimize asynchronous logic, which is why most flow control occurs through events and actions instead of, for example, Reactive extensions. 



