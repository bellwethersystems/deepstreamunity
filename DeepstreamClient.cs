﻿using System;
using System.IO;
using System.Net.Sockets;

namespace UniDs {
	public class DeepstreamClient : IDisposable {

		public LoginSuccessEvent OnLoginSuccess;
		public LoginErrorEvent OnLoginError;

		public readonly RpcService Rpc;
		public readonly EventsService Events;
		public readonly RecordsService Records;

		private Connection Conn;
		
		public DeepstreamClient(string host, int port) {
			UnityEngine.Debug.Log ("DeepstreamClient Connection " + host + ":" + port);
			Conn = new Connection (host, port, "deepstream");
			Conn.OnMessage.AddListener (OnMessageHandler);

			Conn.Connect ();

			OnLoginSuccess = new LoginSuccessEvent ();
			OnLoginError = new LoginErrorEvent ();

			Rpc = new RpcService (Conn);
			Events = new EventsService (Conn);
			Records = new RecordsService (Conn);
		}

		public void Challenge() {
			UnityEngine.Debug.Log ("DeepstreamClient Challenge");
			Conn.SetState (ConnectionState.CHALLENGING);

			Message message = new Message (Topic.CONNECTION, Action.CHALLENGE_RESPONSE);
			message.Payload = new object[1]{ Conn.Host };
			Write (message.ToPackage (true));
		}

		public void Login() {
			Login ("{}");
		}

		public void Login(string credentials) {
//			UnityEngine.Debug.Log ("DeepstreamClient Login: " + Conn.State);
			if (Conn.State != ConnectionState.AWAITING_AUTHENTICATION) {
				return;
			}
			Conn.SetState (ConnectionState.AUTHENTICATING);

			Message message = new Message (Topic.AUTH, Action.REQUEST);
			message.Payload = new object[1]{ credentials };
			Write (message.ToPackage ());
		}

		public void Disconnect() {
			Conn.Disconnect ();
		}

		public void SetConnectionState(ConnectionState state) {
			Conn.SetState (state);
		}

		public void OnMessageHandler(Message message) {
			UnityEngine.Debug.Log ("Reading: " + message.ToString ());

			if (message.Topic == Topic.RPC) {
				Rpc.OnMessage (message);
			} else if (message.Topic == Topic.EVENT) {
				Events.OnMessage (message);
			} else if (message.Topic == Topic.RECORD) {
				Records.OnMessage (message);
			} else if (message.Topic == Topic.CONNECTION) {
				OnConnection (message);
			} else if (message.Topic == Topic.AUTH) {
				OnAuth (message);
			}
		}

		public bool IsConnected() {
			return Conn.IsListening ();
		}

		public void Write (string payload) {
			Message message = new Message (payload);
			Conn.Write (message);
		}

		public bool NeedsAuthentication() {
			return Conn.State == ConnectionState.AWAITING_AUTHENTICATION;
		}

		public bool IsAuthenticated() {
			return Conn.State == ConnectionState.OPEN;
		}

		private void OnConnection(Message message) {
			if (message.Action == UniDs.Action.CHALLENGE) {
				Challenge ();
			} else if (message.Action == UniDs.Action.PING) {
				Pong ();

			} else if (message.Action == UniDs.Action.REDIRECT) {
				// ???

			} else if (message.Action == UniDs.Action.REJECTION) {
				Conn.Disconnect ();

			} else if (message.Action == UniDs.Action.ACK) {
				Conn.SetState (UniDs.ConnectionState.AWAITING_AUTHENTICATION);
			}
		}

		private void OnAuth(Message message) {
			UnityEngine.Debug.Log ("DeepstreamClient OnAuth: " + message);

			if (message.Action == Action.CREATE) {
			} else if (message.Action == Action.ACK) {
				Conn.SetState (ConnectionState.OPEN);
				OnLoginSuccess.Invoke ();
			} else if (message.Action == Action.ERROR) {
				OnLoginError.Invoke ("");
			}
		}

		private void Pong() {
			Message message = new Message (Topic.CONNECTION, Action.PONG);
			Write (message.ToPackage ());
		}

		public void Dispose() {
			UnityEngine.Debug.Log ("DeepstreamClient Dispose");
			Dispose (true);
			GC.SuppressFinalize (this);
		}

		private void Dispose(bool disposing) {
			if (disposing) {
				Conn.Dispose ();

				Rpc.Dispose ();
				Records.Dispose ();
				Events.Dispose ();

				OnLoginSuccess = null;
				OnLoginError = null;
			}
		}
	}
}
