﻿using UnityEngine;
using UnityEngine.Events;

namespace UniDs {
	[System.Serializable]
	public class EventsPublishEvent : UnityEvent<string> {
	}
}