﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UniDs {
	public class EventsService : Service {
		private Dictionary<string, EventsAcknowledgeEvent> OnEventsAcknowledge;
		private Dictionary<string, EventsPublishEvent> OnEventsPublishSubscription;
		private Dictionary<string, EventsPublishEvent> OnEventsPublishListener;

		private Dictionary<string, int> Subscriptions;
		private Dictionary<string, int> Listeners;

		public EventsService(Connection connection) 
			: base(connection) {
			Subscriptions = new Dictionary<string, int>();
			Listeners = new Dictionary<string, int>();

			OnEventsAcknowledge = new Dictionary<string, EventsAcknowledgeEvent> ();
			OnEventsPublishSubscription = new Dictionary<string, EventsPublishEvent> ();
			OnEventsPublishListener = new Dictionary<string, EventsPublishEvent> ();
		}

		public override void OnMessage(Message message) {
			if (message.Action != Action.EVENT)
				return;
			
			EventsPublishEvent callback;

			if (OnEventsPublishSubscription.ContainsKey (message.Name)) {
				OnEventsPublishSubscription.TryGetValue (message.Name, out callback);

				if (callback != null) {
					string payload = message.Payload [0] as string;
					callback.Invoke (payload);
				}
			}

			foreach (KeyValuePair<string, EventsPublishEvent> kv in OnEventsPublishListener) {
				if (message.Name.StartsWith(kv.Key)) {
					OnEventsPublishListener.TryGetValue (message.Name, out callback);

					if (callback != null) {
						string payload = message.Payload [0] as string;
						callback.Invoke (payload);
					}
				}
			}

		}

		public bool IsSubscribed(string eventName) {
			return Subscriptions.ContainsKey (eventName);
		}

		public bool IsListening(string pattern) {
			return Listeners.ContainsKey (pattern);
		}

		public void Subscribe(string eventName) {
			ThrowIfArgumentMissing (eventName);
			ThrowIfConnectionNotOpened ();

			if (IsSubscribed(eventName)) {
				Subscriptions[eventName]++;
			} else {
				Message message = new Message (Topic.EVENT, Action.SUBSCRIBE);
				message.Payload = new object[1]{ eventName };
				this.Connection.Write (message);

				EventsPublishEvent ev = new EventsPublishEvent ();
				OnEventsPublishSubscription.Add (eventName, ev);

				Subscriptions.Add (eventName, 1);
			}
		}

		public void UnSubscribe(string eventName) {
			ThrowIfArgumentMissing (eventName);
			ThrowIfConnectionNotOpened ();

			if (!IsSubscribed(eventName))
				throw new DeepStreamException (Constants.Errors.NOT_SUBSCRIBED);

			Subscriptions[eventName]--;

			if (Subscriptions.Count < 1) {
				Message message = new Message (Topic.EVENT, Action.UNSUBSCRIBE);
				message.Payload = new object[1]{ eventName };
				this.Connection.Write (message);

				OnEventsPublishSubscription.Remove (eventName);
			}
		}

		public void Publish(string eventName) {
			Publish (eventName, null);
		}

		public void Publish(string eventName, string payload) {
			ThrowIfArgumentMissing (eventName);
			ThrowIfConnectionNotOpened ();

			Message message = new Message (Topic.EVENT, Action.EVENT);
			if (string.IsNullOrEmpty (payload)) {
				message.Payload = new object[1]{ eventName };
			} else {
				message.Payload = new object[2]{ eventName, payload };
			}
			this.Connection.Write (message);
		}

		public void Listen(string pattern) {
			ThrowIfArgumentMissing (pattern);
			ThrowIfConnectionNotOpened ();

			if (IsListening(pattern)) {
				Listeners[pattern]++;
			} else {
				Message message = new Message (Topic.EVENT, Action.LISTEN);
				message.Payload = new object[1]{ pattern };
				this.Connection.Write (message);

				EventsPublishEvent ev = new EventsPublishEvent ();
				OnEventsPublishListener.Add (pattern, ev);

				Listeners.Add (pattern, 1);
			}
		}

		public void Unlisten(string pattern) {
			ThrowIfArgumentMissing (pattern);
			ThrowIfConnectionNotOpened ();

			if (!IsListening(pattern))
				throw new DeepStreamException (Constants.Errors.NOT_LISTENING);
			
			Listeners[pattern]--;

			if (Listeners.Count < 1) {
				Message message = new Message (Topic.EVENT, Action.UNLISTEN);
				message.Payload = new object[1]{ pattern };
				this.Connection.Write (message);

				OnEventsPublishListener.Remove (pattern);
			}
		}

		private void Dispose(bool disposing) {
			if (disposing) {
				ArrayList subKeys = new ArrayList(OnEventsPublishSubscription.Keys);
				foreach (string key in subKeys) {
					OnEventsPublishSubscription [key] = null;
					OnEventsPublishSubscription.Remove (key);
				}

				ArrayList listenKeys = new ArrayList(OnEventsPublishListener.Keys);
				foreach (string key in listenKeys) {
					OnEventsPublishListener [key] = null;
					OnEventsPublishListener.Remove (key);
				}

				ArrayList ackKeys = new ArrayList(OnEventsAcknowledge.Keys);
				foreach (string key in ackKeys) {
					OnEventsAcknowledge [key] = null;
					OnEventsAcknowledge.Remove (key);
				}
			}
		}
	}
}