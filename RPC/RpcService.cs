﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

namespace UniDs {
	public class RpcService : Service {
		private Dictionary<string, RpcResponseEvent> OnRpcResponse;
		private Dictionary<string, RpcAcknowledgeEvent> OnRpcAcknowledge;
		private Dictionary<string, RpcRequestErrorEvent> OnRpcError;

		public RpcService(Connection connection) 
			: base(connection) {
			OnRpcResponse = new Dictionary<string, RpcResponseEvent> ();
			OnRpcAcknowledge = new Dictionary<string, RpcAcknowledgeEvent> ();
			OnRpcError = new Dictionary<string, RpcRequestErrorEvent> ();
		}

		public override void OnMessage(Message message) {

			if (message.Action == Action.ACK) {

			} else if (message.Action == Action.RESPONSE) {
				RpcResponseEvent callback;

				if (OnRpcResponse.ContainsKey (message.Uid)) {
					OnRpcResponse.TryGetValue (message.Uid, out callback);

					if (callback != null) {
						string payload = message.Payload [0] as string;
						RpcEventResult args = new RpcEventResult (message.Uid, message.Name, payload);

						callback.Invoke (args);
					}
				}
				RemoveCallbacks (message.Uid);

			} else if (message.Action == Action.REQUEST) {
				throw new NotImplementedException ("RPC Request not implemented");

			} else if (message.Action == Action.REJECTION) {
				if (OnRpcResponse.ContainsKey (message.Uid)) {
					OnRpcResponse.Remove (message.Uid);
				}
				RemoveCallbacks (message.Uid);
				
			} else if (message.Action == Action.ERROR) {
				RpcRequestErrorEvent callback;

				if (OnRpcError.ContainsKey (message.Uid)) {
					OnRpcError.TryGetValue (message.Uid, out callback);

					if (callback != null) {
						RpcEventResult args = new RpcEventResult (message.Uid, message.Name);
						callback.Invoke (args);
					}
				}
				RemoveCallbacks (message.Uid);
			}
			
		}

		public Message Make(string rpcName) {
			return Make (rpcName, "{}", null, null);
		}

		public Message Make(string rpcName, string payload) {
			return Make (rpcName, payload, null, null);
		}

		public Message Make(string rpcName, string payload, UnityAction<RpcEventResult> successCallback) {
			return Make (rpcName, payload, successCallback, null);
		}

		public Message Make(string rpcName, string payload, UnityAction<RpcEventResult> successCallback, UnityAction<RpcEventResult> errorCallback) {
			ThrowIfArgumentMissing (rpcName);
			ThrowIfConnectionNotOpened ();

			Message message = new Message (Topic.RPC, Action.REQUEST);
			string typedPayload = string.Format ("{0}{1}", "O", payload);
			message.Payload = new string[3]{ rpcName, message.Uid, typedPayload };

			this.Connection.Write (message);

			if (successCallback != null) {
				RpcResponseEvent ev = new RpcResponseEvent ();
				ev.AddListener (successCallback);
				OnRpcResponse.Add (message.Uid, ev);
			}
			if (errorCallback != null) {
				RpcRequestErrorEvent ev = new RpcRequestErrorEvent ();
				ev.AddListener (errorCallback);
				OnRpcError.Add (message.Uid, ev);
			}

			return message;
		}

		private void RemoveCallbacks(string key) {
			if (OnRpcResponse.ContainsKey (key)) {
				OnRpcResponse [key] = null;
				OnRpcResponse.Remove (key);
			}
			if (OnRpcAcknowledge.ContainsKey (key)) {
				OnRpcAcknowledge [key] = null;
				OnRpcAcknowledge.Remove (key);
			}
			if (OnRpcError.ContainsKey (key)) {
				OnRpcError [key] = null;
				OnRpcError.Remove (key);
			}
		}

		private void Dispose(bool disposing) {
			if (disposing) {
				ArrayList resKeys = new ArrayList(OnRpcResponse.Keys);
				foreach (string key in resKeys) {
					OnRpcResponse [key] = null;
					OnRpcResponse.Remove (key);
				}

				ArrayList ackKeys = new ArrayList(OnRpcAcknowledge.Keys);
				foreach (string key in ackKeys) {
					OnRpcAcknowledge [key] = null;
					OnRpcAcknowledge.Remove (key);
				}

				ArrayList errKeys = new ArrayList(OnRpcError.Keys);
				foreach (string key in errKeys) {
					OnRpcError [key] = null;
					OnRpcError.Remove (key);
				}
			}
		}
	}
}